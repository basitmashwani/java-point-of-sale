//    uniCenta oPOS  - Touch Friendly Point Of Sale
//    Copyright (c) 2009-2012 uniCenta
//    http://www.unicenta.net/unicentaopos
//
//    This file is part of uniCenta oPOS
//
//    uniCenta oPOS is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//   uniCenta oPOS is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with uniCenta oPOS.  If not, see <http://www.gnu.org/licenses/>

report = new com.openbravo.pos.reports.PanelReportBean();

report.setTitleKey("Menu.CategorySales");
report.setReport("/com/openbravo/reports/categorysales");
report.setResourceBundle("com/openbravo/reports/categorysales_messages");

report.setSentence(
            "SELECT " +
		"CATEGORIES.NAME, " +
                "((SELECT SUM(TICKETLINES.UNITS) FROM TICKETLINES WHERE TICKETLINES.PRODUCT IS NOT NULL)) AS QTY, " +
                "((SELECT SUM(TICKETLINES.PRICE * TICKETLINES.UNITS) FROM TICKETLINES WHERE TICKETLINES.PRODUCT IS NULL)) AS DISC, " +             
		"SUM(TICKETLINES.PRICE * TICKETLINES.UNITS) AS CATPRICE, " +
		"SUM((TICKETLINES.PRICE * TAXES.RATE ) * TICKETLINES.UNITS) AS CATTAX, "+
		"SUM((TICKETLINES.PRICE + TICKETLINES.PRICE * TAXES.RATE ) * TICKETLINES.UNITS) AS CATTOTAL " +
            "FROM TICKETLINES, TICKETS, RECEIPTS, TAXES, PRODUCTS, CATEGORIES " +
            "WHERE TICKETLINES.TICKET = TICKETS.ID " +
		"AND TICKETS.ID = RECEIPTS.ID " +
		"AND TICKETLINES.TAXID = TAXES.ID " +
		"AND ?(QBF_FILTER) " +
          "GROUP BY CATEGORIES.NAME " +
          "ORDER BY CATEGORIES.NAME");

report.addParameter("RECEIPTS.DATENEW");
report.addParameter("RECEIPTS.DATENEW");

paramdates = new com.openbravo.pos.reports.JParamsDatesInterval();
paramdates.setEndDate(com.openbravo.beans.DateUtils.getToday());

report.addQBFFilter(paramdates);

report.addField("NAME", com.openbravo.data.loader.Datas.STRING);
report.addField("QTY", com.openbravo.data.loader.Datas.STRING);
report.addField("DISC", com.openbravo.data.loader.Datas.DOUBLE);
report.addField("CATPRICE", com.openbravo.data.loader.Datas.DOUBLE);
report.addField("CATTAX", com.openbravo.data.loader.Datas.DOUBLE);
report.addField("CATTOTAL", com.openbravo.data.loader.Datas.DOUBLE);

report;