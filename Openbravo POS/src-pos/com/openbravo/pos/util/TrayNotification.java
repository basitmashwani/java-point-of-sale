/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.openbravo.pos.util;

import java.awt.AWTEventMulticaster;
import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.TrayIcon.MessageType;
import java.awt.event.MouseAdapter;
import javax.swing.ImageIcon;

/**
 *
 * @author Syed Abdul Basit
 */
public class TrayNotification {
    
    public void displayTray(String member) throws AWTException{
        //Obtain only one instance of the SystemTray object
        SystemTray tray = SystemTray.getSystemTray();
        //Creating a tray icon
        ImageIcon icon = new ImageIcon(getClass().getResource("app_lancher-mdpi.png"));
        java.awt.Image image = icon.getImage();
          TrayIcon trayIcon = new TrayIcon(image, "Tray Demo");
        //Let the system resizes the image if needed
        trayIcon.setImageAutoSize(true);
        //Set tooltip text for the tray icon
        trayIcon.setToolTip("Email Notification");
        tray.add(trayIcon);
        trayIcon.displayMessage("Email Notification", "Email Succesfully Send to "+member, MessageType.INFO);
    
    }
    
}
